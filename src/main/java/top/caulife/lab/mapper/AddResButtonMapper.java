package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;

@Repository
public interface AddResButtonMapper {
    void addresbuttonjs(int unitexpstepno, int unitno, String type, int resno, int resnum);
    void delresbuttonjs(int delunitexpstepno, int delunitno, String delrestype, int delresno);
}
