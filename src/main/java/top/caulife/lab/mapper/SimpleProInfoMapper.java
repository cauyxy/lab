package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.SimpleProInfo;

import java.util.List;

@Repository
public interface SimpleProInfoMapper {
    List<SimpleProInfo> sel(String pi_id);

    void insertNew(SimpleProInfo simpleProInfo);
}
