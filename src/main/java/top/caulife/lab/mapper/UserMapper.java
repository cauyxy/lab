package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.User;
import top.caulife.lab.entity.UserPermission;
import top.caulife.lab.entity.User_jys;

@Repository
public interface UserMapper {

    User login(String username,String password);
    User_jys login_jys(String username, String password);
    UserPermission login_jys_permission(int usertid);
}
