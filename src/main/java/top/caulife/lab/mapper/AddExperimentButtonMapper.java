package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;

@Repository
public interface AddExperimentButtonMapper {
    void addexperimentbuttonjs(String projectno, String projectname, int experimentno, String experimentname, int PIno, int assistantno);
    void delexperimentbuttonjs(int experimentno);
}
