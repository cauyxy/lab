package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.PIInfo;

import java.util.List;


@Repository
public interface PIInfoMapper {

    List<PIInfo> sel(String pi_id);
}
