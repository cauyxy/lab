package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.FuncUnit;

@Repository
public interface FuncUnitMapper {

    FuncUnit sel(String pro_id);

    void updateState(String pro_id,String state);

    void updateEquInfo(String pro_id,String equ_info);

    void updateAnimalInfo(String pro_id,String animal_info);

    void updateDrugInfo(String pro_id,String drug_info);

    void updateSampleInfo(String pro_id,String sample_info);
}
