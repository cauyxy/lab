package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.GetAddStepNo;

import java.util.List;

@Repository
public interface GetAddStepNoMapper {
   List<GetAddStepNo> getaddstepnojs();
//    CheckStepState checkstepstatejs(int stepno);
}