package top.caulife.lab.mapper;

import top.caulife.lab.entity.CheckUnitState;
import org.springframework.stereotype.Repository;

@Repository
public interface CheckUnitStateMapper {
    CheckUnitState checkunitstatejs(String unitno);
}
