package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.bookingTime.StepIden;
import top.caulife.lab.entity.bookingTime.UnitIden;
import top.caulife.lab.entity.bookingTime.UnitState;

import java.util.List;

@Repository
public interface UnitIdenMapper {
    List<StepIden> getStepIden(int experiment_id);
    List<UnitIden> getUnitIden(int experiment_id, int step_id, int step_priority);
    UnitState getUnitState(String pro_id);
}
