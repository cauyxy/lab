package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.bookingMessage.ExpMessage;

import java.util.List;

@Repository
public interface ExpMesMapper {
    List<ExpMessage> getExpMessageById(int user_id);
    List<ExpMessage> getAllExpMessage();
    void delExpById(int experiment_id);
    void changeExpStateById(int experiment_id, String progress_id);
}
