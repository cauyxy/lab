package top.caulife.lab.mapper;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.ProPartialInfo;

import java.util.Date;
import java.util.List;

@Repository
public interface ProPartialInfoMapper {
    List<ProPartialInfo> sel(String exp_id, String pro_id, String pro_name, String pro_state);
    public int updateState(String exp_id, String pro_id, String pro_state, String other_info);
    List<ProPartialInfo> judge_authorization(String exp_id, String pro_id, String pi_id,
                                                    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss") Date cur_time);
    int updateCheckLog(String pro_id, String checklog_id);
    public int allDelete(String exp_id, @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss") Date cur_time, String pro_state, String other_info);
}
