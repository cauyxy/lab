package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.GetOneStepNo;

import java.util.List;

@Repository
public interface GetOneStepNoMapper {
   List<GetOneStepNo> getonestepnojs();
//    CheckStepState checkstepstatejs(int stepno);
}