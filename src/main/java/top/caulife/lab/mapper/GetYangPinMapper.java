package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.YangPin;

import java.util.List;

@Repository
public interface GetYangPinMapper {
   List<YangPin> getyangpinjs();
//    CheckStepState checkstepstatejs(int stepno);
}
