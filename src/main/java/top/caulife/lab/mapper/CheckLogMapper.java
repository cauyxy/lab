package top.caulife.lab.mapper;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.CheckLog;

import java.util.Date;
import java.util.List;


@Repository
public interface CheckLogMapper {

    List<CheckLog> sel(String checklog_id);

    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    void record(String checklog_id, String checker_id,Date check_time, String check_result);
}
