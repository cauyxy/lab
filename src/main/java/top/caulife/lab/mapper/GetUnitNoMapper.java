package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.GetUnitNo;

import java.util.List;

@Repository
public interface GetUnitNoMapper {
   List<GetUnitNo> getunitnojs(String unitno);
//    CheckStepState checkstepstatejs(int stepno);
}