package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.bookingMessage.ResMessage;

import java.util.List;

@Repository
public interface ResourceMapper {
    List<ResMessage> getUnitStaticMaterialMessage(int unit_id);
    List<ResMessage> getUnitStaticEquipmentMessage(int unit_id);

    List<ResMessage> getUnitDynamicResMessage(int exp_id, int step_id, int step_priority, int unit_id, String type);
}
