package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.CheckStepState;

import java.util.List;

@Repository
public interface CheckStepStateMapper {
   List<CheckStepState> checkstepstatejs(int stepno);
//    CheckStepState checkstepstatejs(int stepno);
}
