package top.caulife.lab.mapper;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;
//import top.caulife.lab.entity.LoginLabLog;
import top.caulife.lab.entity.LoginLabLog;

import java.util.Date;
import java.util.List;

@Repository
public interface LoginLabLogMapper {
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    void record(int uid, String username,String role, Date loginlab_time);
    List<LoginLabLog> sel(String username,String role,String start_time,String end_time);
    void del(int loginlab_id);
}
