package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.GetAddUnitNo;

import java.util.List;

@Repository
public interface GetAddUnitNoMapper {
   List<GetAddUnitNo> getaddunitnojs();
//    CheckStepState checkstepstatejs(int stepno);
}