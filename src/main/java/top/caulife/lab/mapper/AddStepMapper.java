package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;

import top.caulife.lab.entity.AddStep;

@Repository
public interface AddStepMapper {
    void addstepjs(int stepno, String stepname,String stepmess);
}
