package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.bookingMessage.UnitTime;
import top.caulife.lab.entity.bookingTime.StepIden;

import java.util.List;


@Repository
public interface UpdExpMapper {
    void updExpMessageById(int exp_id, String exp_name, int pi_id, int as_id);
//    void getUnitTime(int exp_id, String exp_name, int pi_id, int as_id);
    UnitTime getUnitTime(int exp_id, int step_id, int step_pr, int unit_id, int unit_pr);
    void addUnitTime(int exp_step_id, int unit_id, int unit_pr);
    StepIden selUnitTime(int exp_id, int step_id, int step_pr);
    void updUnitTime(int exp_step_id, int unit_id, int unit_pr, int unit_hour);
}
