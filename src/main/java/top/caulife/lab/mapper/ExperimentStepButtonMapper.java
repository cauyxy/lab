package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.ExperimentStep;

import java.util.List;

@Repository
public interface ExperimentStepButtonMapper {
   List<ExperimentStep> getexperimentstepjs(int experimentno);
//    CheckStepState checkstepstatejs(int stepno);
}
