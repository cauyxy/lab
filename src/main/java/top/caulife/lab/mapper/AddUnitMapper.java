package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;

import top.caulife.lab.entity.AddUnit;

@Repository
public interface AddUnitMapper {
    void addunitjs(int stepno, int unitno, int priority);
}
