package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.YangPin;

import java.util.List;

@Repository
public interface GetYiQiMapper {
   List<YangPin> getyiqijs();
//    CheckStepState checkstepstatejs(int stepno);
}
