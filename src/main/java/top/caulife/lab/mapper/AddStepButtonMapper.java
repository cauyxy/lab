package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;

@Repository
public interface AddStepButtonMapper {
    void addstepbuttonjs(int experimentno, int stepno, int steppriority, int steptime);
    void delstepbuttonjs(int delsteono);
}
