package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.StepNoName;

import java.util.List;

@Repository
public interface StepNoNameMapper {
   List<StepNoName> stepnonamejs();
//    CheckStepState checkstepstatejs(int stepno);
}
