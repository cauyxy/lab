package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.DataManageInfo;

import java.util.List;

@Repository
public interface DataManageInfoMapper {

    List<DataManageInfo> sel();

    void insert(DataManageInfo dataManageInfo);
    void delete(DataManageInfo dataManageInfo);
}
