package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.PIAS;

import java.util.List;

@Repository
public interface PIASMapper {
   List<PIAS> piasjs();
//    CheckStepState checkstepstatejs(int stepno);
}
