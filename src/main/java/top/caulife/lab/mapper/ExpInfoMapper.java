package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.User;
import top.caulife.lab.entity.UserPermission;
import top.caulife.lab.entity.User_jys;
import top.caulife.lab.entity.bookingMessage.ExpInfo;

@Repository
public interface ExpInfoMapper {
    ExpInfo getExpInfo(int exp_id, int step_id, int step_pr, int unit_id, int unit_pr);
}
