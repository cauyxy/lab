package top.caulife.lab.mapper;

import org.springframework.stereotype.Repository;
import top.caulife.lab.entity.ExpFileInfo;

@Repository
public interface ExpFileInfoMapper {

    ExpFileInfo sel(String id);

    void updateReport(String id,String file_report);

    void updateData(String id, String file_data);

    void updateExtra(String id, String file_extra);

    String getPiName(int pi_id);

    void insertRecord(int String,String func_name, String exp_name, String pro_name);
}
