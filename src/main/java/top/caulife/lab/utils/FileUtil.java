package top.caulife.lab.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtil {

    /***
     * 文件下载
     * @param filename 需要下载文件的绝对路径
     * @param res 所需请求
     * @throws IOException 抛出IO错误
     */
    public static void download(String filename, HttpServletResponse res,String origin_name) throws IOException {
        InputStream inputStream = null;
        OutputStream os = null;
        try{
            File file = new File(filename);
            inputStream = new FileInputStream(file);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len =0;
            while((len=inputStream.read(buffer))!=-1){
                outStream.write(buffer,0,len);
            }
            inputStream.close();
            byte[] data = outStream.toByteArray();
            res.reset();
            res.addHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(origin_name,"UTF-8"));
            res.setContentType("application/octet-stream");
            os = res.getOutputStream();
            os.write(data);
            os.flush();
            os.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(inputStream!=null){
                inputStream.close();
            }
            if(os!=null){
                os.close();
            }
        }
    }

    /**
     * 功能:压缩多个文件，输出压缩后的zip文件流
     * @param srcfile：源文件列表
     * @param zipFileName：压缩后的文件名
     * @param response: Http响应
     */
    public static void zipFiles(HttpServletResponse response,List<String> name_lis,List<String> srcfile, String zipFileName) {
        byte[] buf = new byte[1024];
        // 获取输出流
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            response.reset();
            // 不同类型的文件对应不同的MIME类型
            response.setContentType("application/x-msdownload");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename=" + zipFileName + ".zip");

            assert bos != null;
            ZipOutputStream out = new ZipOutputStream(bos);

            for (int i=0;i<srcfile.size();i++) {
                FileInputStream in = new FileInputStream(srcfile.get(i));
                out.putNextEntry(new ZipEntry(name_lis.get(i)));
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                out.closeEntry();
                in.close();
            }

            out.close();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}