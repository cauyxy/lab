package top.caulife.lab.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class LoginLabLog {
    int loginlab_id;
    int uid;
    String username;
    String role;
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    Date loginlab_time;
}
