package top.caulife.lab.entity;

import lombok.Data;

@Data
public class CheckUnitState {
    String unitno;
    String unitstate;
}
