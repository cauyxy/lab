package top.caulife.lab.entity;

import lombok.Data;

@Data
public class ExperimentStep {
    int experimentstepno;
    int stepno;
    int steppriority;
    int steptime;
}
