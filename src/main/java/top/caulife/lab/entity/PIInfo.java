package top.caulife.lab.entity;

import lombok.Data;

@Data
public class PIInfo {
    String pi_id;
    String pi_name;
    String pi_title;
    String pi_gender;
}
