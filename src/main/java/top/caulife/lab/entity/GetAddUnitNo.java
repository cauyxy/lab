package top.caulife.lab.entity;

import lombok.Data;

@Data
public class GetAddUnitNo {
    int unitno;
    String unitname;
}