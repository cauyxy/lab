package top.caulife.lab.entity;

import lombok.Data;

@Data
public class UserPermission {
    int booking_business;
    int experimental_business;
    int process_tracing;
    int step_management;
}
