package top.caulife.lab.entity.bookingTime;


import lombok.AllArgsConstructor;
import lombok.Data;

// 申请预约资源的回复
@Data
@AllArgsConstructor
public class OrderTimeReply {
    public String unit_id_str;         // 与请求的相同，功能单元ID
    public ResourceType resource_type; // 与请求的相同，资源类型
    public int resource_id;            // 与请求的相同，资源ID
    public boolean isOK;               // 是否预约成功
}
