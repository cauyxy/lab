package top.caulife.lab.entity.bookingTime;
import lombok.Data;

@Data
public class StepIden {
    int experiment_id;
    int exp_step_id;
    int step_id;
    int step_priority;
    String step_name;
    int step_time_hour;
}
