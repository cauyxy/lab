package top.caulife.lab.entity.bookingTime;


import lombok.AllArgsConstructor;
import lombok.Data;

// 资源信息类
@Data
@AllArgsConstructor
public class Resource {
    public int resource_id;     // 资源编号
    public String name;         // 资源名
    public String description;  // 资源描述
}
