package top.caulife.lab.entity.bookingTime;

import java.util.Date;

// 申请预约资源
public class OrderTime {
    public String unit_id_str;         // 功能单元ID
    public ResourceType resource_type; // 资源类型
    public int resource_id;            // 资源ID
    public int num;                    // 数量
    public Date startTime;             // 开始使用的时间
    public int hour;                   // 使用时长
}
