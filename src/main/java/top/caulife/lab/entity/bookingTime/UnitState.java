package top.caulife.lab.entity.bookingTime;
import lombok.Data;

@Data
public class UnitState {
    String progress_id;
    String progress_description;
    String progress_type;
}
