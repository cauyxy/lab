package top.caulife.lab.entity.bookingTime;


import lombok.AllArgsConstructor;
import lombok.Data;

// 查询资源可用时间的回复
@Data
@AllArgsConstructor
public class QueryTimeReply {
    public String unit_id_str;         // 与请求的相同，功能单元ID
    public ResourceType resource_type; // 与请求的相同，资源类型
    public int resource_id;            // 与请求的相同，资源ID
    public TimeSlot timeSlot;          // 可以开始使用的时间段
}
