package top.caulife.lab.entity.bookingTime;

public enum ResourceType {
    ANIMAL,     // 动物
    MATERIAL,   // 试剂耗材
    EQUIPMENT,  // 设备
    SAMPLE      // 生物样本
}
