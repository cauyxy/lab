package top.caulife.lab.entity.bookingTime;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

// 查询某类资源的列表的回复
@Data
@AllArgsConstructor
public class ResourceListReply {
    public ResourceType resource_type;  // 资源类型
    public List<Resource> resources;    // 资源列表
}
