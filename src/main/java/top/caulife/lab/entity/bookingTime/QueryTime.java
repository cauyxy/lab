package top.caulife.lab.entity.bookingTime;


// 查询资源可用时间
public class QueryTime {
    public String unit_id_str;         // 功能单元ID - UN/X
    public ResourceType resource_type; // 资源类型
    public int resource_id;            // 资源ID
    public int num;                    // 数量
    public int hour;                   // 使用时长

    public void setUnit_id_str(int exp_id, int step_id, int step_pr, int unit_id, int unit_pr) {
        this.unit_id_str = String.format(
                "UN%d-%d-%d-%d-%d",
                exp_id,
                step_id,
                step_pr,
                unit_id,
                unit_pr
        );
    }
}
