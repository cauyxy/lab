package top.caulife.lab.entity.bookingTime;
import lombok.Data;

@Data
public class UnitIden {
    int experiment_id;
    int step_id;
    int step_priority;
    int unit_id;
    int unit_priority;
    String unit_name;

    public static String getId(UnitIden ui) {
        return String.format(
                "UN%d-%d-%d-%d-%d",
                ui.getExperiment_id(),
                ui.getStep_id(),
                ui.getStep_priority(),
                ui.getUnit_id(),
                ui.getUnit_priority()
        );
    }

    public static UnitIden setId(String id) {
        UnitIden ui = new UnitIden();
        String[] ids = id.substring(2).split("-");
        ui.experiment_id = Integer.parseInt(ids[0]);
        ui.step_id = Integer.parseInt(ids[1]);
        ui.step_priority= Integer.parseInt(ids[2]);
        ui.unit_id = Integer.parseInt(ids[3]);
        ui.unit_priority = Integer.parseInt(ids[4]);
        return ui;
    }
}
