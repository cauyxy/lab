package top.caulife.lab.entity;

import lombok.Data;

@Data
public class Response {

    private int code;
    private String msg;
    private Object data;

    public static Response success(){
        Response res = new Response();
        res.setCode(1);
        return res;
    }

    public static Response fail(){
        Response res = new Response();
        res.setCode(0);

        return res;
    }

    public static Response success(String msg){
        Response res = new Response();
        res.setCode(1);
        res.setMsg(msg);

        return res;
    }

    public static Response fail(String msg){
        Response res = new Response();
        res.setCode(0);
        res.setMsg(msg);

        return res;
    }

    public static Response success(Object data){
        Response res = new Response();
        res.setCode(1);
        res.setData(data);

        return res;
    }

    public static Response fail(Object data){
        Response res = new Response();
        res.setCode(0);
        res.setData(data);

        return res;
    }

    public static Response success(String msg,Object data){
        Response res = new Response();
        res.setCode(1);
        res.setMsg(msg);
        res.setData(data);

        return res;
    }

    public static Response fail(String msg,Object data){
        Response res = new Response();
        res.setCode(0);
        res.setMsg(msg);
        res.setData(data);

        return res;
    }
}
