package top.caulife.lab.entity;

import lombok.Data;

@Data
public class GetAddStepNo {
    int stepno;
}