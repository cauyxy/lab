package top.caulife.lab.entity;

import lombok.Data;

@Data
public class AddStep {
    int stepno;
    String stepname;
    String stepmess;
}
