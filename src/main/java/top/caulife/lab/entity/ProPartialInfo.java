package top.caulife.lab.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class ProPartialInfo {
    String exp_id;
    String pro_id;
    String PI_id;
    String pro_name;
    String pro_state;
    String other_info;
    String checklog_id;
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    Date pro_start_time;
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    Date pro_end_time;
}
