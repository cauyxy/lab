package top.caulife.lab.entity;

import lombok.Data;

@Data
public class PIAS {
    int user_id;
    String user_name;
    int user_type;
    String project_id;
}
