package top.caulife.lab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataManageInfo {
    String RecordID;
    String RecordName;
    String RecordDate;
}
