package top.caulife.lab.entity.bookingMessage;
import lombok.Data;

@Data
public class UnitMessage {
    public int id;
    public String name;
    public int priority;
    public String progress_id;
    public String progress_description;
    public String progress_type;
}
