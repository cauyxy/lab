package top.caulife.lab.entity.bookingMessage;
import lombok.Data;

@Data
public class ResMessage {
    String res_type;
    int res_id;
    int res_num;
    String res_name;
    String res_mess;
    boolean must;
}
