package top.caulife.lab.entity.bookingMessage;
import java.util.Date;
import lombok.Data;

@Data
public class UnitTime {
    int exp_id;
    int step_id;
    int step_pr;
    int unit_id;
    int unit_pr;
    int unit_hour;
    String unit_id_str;

    public void setUnit_id_str() {
        this.unit_id_str = String.format(
                "UN%d-%d-%d-%d-%d",
                getExp_id(),
                getStep_id(),
                getStep_pr(),
                getUnit_id(),
                getUnit_pr()
        );
    }
}
