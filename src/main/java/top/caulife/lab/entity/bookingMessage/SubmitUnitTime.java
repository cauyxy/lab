package top.caulife.lab.entity.bookingMessage;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class SubmitUnitTime {
    int exp_id;
    int step_id;
    int step_pr;
    int unit_id;
    int unit_pr;
    String unit_time;

    public Date getUnit_date() throws ParseException {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return dateFormatter.parse(unit_time);
    }

    public String getUnit_id_str() {
        return String.format(
                "UN%d-%d-%d-%d-%d",
                getExp_id(),
                getStep_id(),
                getStep_pr(),
                getUnit_id(),
                getUnit_pr()
        );
    }
}
