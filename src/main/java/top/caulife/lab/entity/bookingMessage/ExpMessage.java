package top.caulife.lab.entity.bookingMessage;
import lombok.Data;

@Data
public class ExpMessage {
    int experiment_id;
    String experiment_name;
    String project_id;
    String project_name;
    int independent_pi_id;
    int lab_assistant_id;
    String independent_pi_name;
    String lab_assistant_name;
    String pre_progress_id;
}
