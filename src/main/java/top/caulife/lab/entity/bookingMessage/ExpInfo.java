package top.caulife.lab.entity.bookingMessage;
import lombok.Data;

@Data
public class ExpInfo {
    public String unit_name;
    public String exp_name;
    public int pi_id;
    public int as_id;
    public String pj_id;
    public String pj_name;
}
