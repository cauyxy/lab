package top.caulife.lab.entity;

import lombok.Data;

@Data
public class AddResButton {
    int unitexpstepno;
    int unitno;
    String type;
    int resno;
    int resnum;
}
