package top.caulife.lab.entity;

import lombok.Data;

@Data
public class AddStepButton {
    int experimentstepno;
    int experimentno;
    int stepno;
    int steppriority;
    int steptime;
}
