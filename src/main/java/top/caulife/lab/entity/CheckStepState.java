package top.caulife.lab.entity;

import lombok.Data;

@Data
public class CheckStepState {
    String unitno;
    String unitstate;
}
