package top.caulife.lab.entity;

import lombok.Data;

@Data
public class AddUnit {
    int stepno;
    int unitno;
    int priority;
}
