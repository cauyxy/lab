package top.caulife.lab.entity;

import lombok.Data;

@Data
public class FuncUnit {
    String pro_id;
    String pro_name;
    String pro_state;
    String equ_info;
    String animal_info;
    String drug_info;
    String sample_info;
}
