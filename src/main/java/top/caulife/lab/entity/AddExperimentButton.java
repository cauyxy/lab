package top.caulife.lab.entity;

import lombok.Data;

@Data
public class AddExperimentButton {
    String projectno;
    String projectname;
    int experimentno;
    String experimentname;
    int PIno;
    int assistantno;
}
