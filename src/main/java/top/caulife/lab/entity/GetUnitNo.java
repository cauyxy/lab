package top.caulife.lab.entity;

import lombok.Data;

@Data
public class GetUnitNo {
    String unitno;
}