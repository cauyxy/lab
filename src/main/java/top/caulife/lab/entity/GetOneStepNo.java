package top.caulife.lab.entity;

import lombok.Data;

@Data
public class GetOneStepNo {
    int stepno;
}