package top.caulife.lab.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CheckLog {
    String checklog_id;
    String checker_id;
    Date check_time;
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    String check_result;
}
