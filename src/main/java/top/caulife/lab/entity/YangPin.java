package top.caulife.lab.entity;

import lombok.Data;

@Data
public class YangPin {
    int yangpinid;
    String yangpinname;
}
