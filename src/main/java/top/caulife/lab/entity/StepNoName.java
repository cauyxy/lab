package top.caulife.lab.entity;

import lombok.Data;

@Data
public class StepNoName {
    int stepno;
    String stepname;
}
