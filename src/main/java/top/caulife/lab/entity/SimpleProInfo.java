package top.caulife.lab.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class SimpleProInfo {
    String exp_id;
    String pro_id;
    String project_id;
    String pi_id;
    String pro_name;
    String exp_name;
    String project_name;
    String pro_state;

    String assist_id;

    String equ_info;
    String animal_info;
    String drug_info;
    String sample_info;
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    Date pro_start_time;
}
