package top.caulife.lab.entity;

import lombok.Data;

@Data
public class User {
    int uid;
    String username;
    String password;
    String role;
}
