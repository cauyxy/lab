package top.caulife.lab.entity;

import lombok.Data;

@Data
public class ExpFileInfo {
    String pro_id;
    String pro_name;
    String exp_name;
    String project_name;
    int pi_id;
    String pi_name;
    String pro_state;
    String file_report;
    String file_data;
    String file_extra;
}
