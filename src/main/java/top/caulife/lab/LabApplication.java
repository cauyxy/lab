package top.caulife.lab;

import org.apache.catalina.connector.Connector;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;

import java.io.File;

@SpringBootApplication
@MapperScan("top.caulife.lab.mapper")
public class LabApplication {

    public static void main(String[] args) {
        env_run();
        SpringApplication.run(LabApplication.class, args);
    }

    static void env_run(){
        File path = new File("./files/data/");
        if (!path.exists()){
            path.mkdirs();
        }
        path = new File("./files/report/");
        if (!path.exists()){
            path.mkdirs();
        }
        path = new File("./files/extra/");
        if (!path.exists()){
            path.mkdirs();
        }
    }

    @Bean
    public ConfigurableServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
            @Override
            public void customize(Connector connector) {
                connector.setProperty("relaxedQueryChars", "|{}[]");
            }
        });
        return factory;
    }
}
