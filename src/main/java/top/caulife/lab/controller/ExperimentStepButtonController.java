package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.ExperimentStep;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.ExperimentStepButtonService;

import java.util.List;

@RestController
public class ExperimentStepButtonController {
    @Autowired
    ExperimentStepButtonService experimentstepbuttonService;

    @RequestMapping("/getexperimentstepjs")
    public Response getexperimentstepjs(@RequestParam("experimentno") int experimentno){
        List<ExperimentStep> getexperimentstep = experimentstepbuttonService.getexperimentstepjs(experimentno);
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
        return Response.success(getexperimentstep);
//        }
//        return Response.fail((Object) null);
    }

}
