package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.YangPin;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.GetYangPinService;

import java.util.List;

@RestController
public class GetYangPinController {
    @Autowired
    GetYangPinService getyangpinService;

    @RequestMapping("/getyangpinjs")
    public Response getyangpinjs(){
        List<YangPin> getyangpin = getyangpinService.getyangpinjs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success(getyangpin);
//        }
//        return Response.fail((Object) null);
    }

}
