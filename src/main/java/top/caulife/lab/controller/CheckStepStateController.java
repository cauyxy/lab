package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.CheckStepState;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.CheckStepStateService;

import java.util.List;

@RestController
public class CheckStepStateController {
    @Autowired
    CheckStepStateService checkstepstateService;

    @RequestMapping("/checkstepstatejs")
    public Response checkstepstatejs(@RequestParam("stepno") int stepno){
        List<CheckStepState> checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success(checkstepstate);
//        }
//        return Response.fail((Object) null);
    }

}
