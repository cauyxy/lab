package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.YangPin;
import top.caulife.lab.service.GetUnitListService;

import java.util.List;

@RestController
public class GetUnitListController {
    @Autowired
    GetUnitListService getunitlistService;

    @RequestMapping("/getunitlistjs")
    public Response getunitlistjs(){
        List<YangPin> getunitlist = getunitlistService.getunitlistjs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success(getunitlist);
//        }
//        return Response.fail((Object) null);
    }

}
