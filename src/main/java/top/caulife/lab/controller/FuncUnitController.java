package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.FuncUnit;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.FuncUnitService;

@RestController
@RequestMapping("/funcUnit")
public class FuncUnitController {
    @Autowired
    FuncUnitService funcUnitService;

    @RequestMapping("/getInfo")
    public Response getInfo(@RequestParam("id") String id) {
        FuncUnit funcUnit = funcUnitService.sel(id);
        if (funcUnit != null) {
            return Response.success(funcUnit);
        }
        return Response.fail();
    }

    @RequestMapping("/updateState")
    public Response updateStatus(@RequestParam("id") String id,
                                 @RequestParam("state") String state) {
        try {
            funcUnitService.updateState(id, state);
            return Response.success();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.fail();
        }
    }

    @RequestMapping("/updateResInfo")
    public Response updateResInfo(@RequestParam("id") String id,
                                  @RequestParam("animal_info") String animal_info,
                                  @RequestParam("equ_info") String equ_info,
                                  @RequestParam("drug_info") String drug_info,
                                  @RequestParam("sample_info") String sample_info) {
        try {
            funcUnitService.updateResInfo(id, animal_info, equ_info, drug_info,sample_info);
            return Response.success();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.fail();
        }
    }
}
