package top.caulife.lab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.DataManageInfo;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.DataManageInfoService;

import java.util.List;

@RestController
@RequestMapping("/dataManageInfo")
public class DataManageInfoController {
    @Autowired
    DataManageInfoService dataManageInfoService;

    @RequestMapping("/getInfo")
    public Response getInfo(){
        List<DataManageInfo> dataManageInfos = dataManageInfoService.sel();
        if (dataManageInfos!=null){
            return Response.success(dataManageInfos);
        }
        return Response.fail();
    }

    @RequestMapping("/insert")
    public Response insert(@RequestParam("recordID")String RecordID,
                           @RequestParam("recordName") String RecordName,
                           @RequestParam("recordDate") String RecordDate){
        try{
            dataManageInfoService.insert(new DataManageInfo(
                    RecordID,RecordName,RecordDate));
            return Response.success();
        }catch (Exception e){
            e.printStackTrace();
            return Response.fail();
        }
    }

    @RequestMapping("/delete")
    public Response delete(@RequestParam("recordID")String RecordID){
        try{
            dataManageInfoService.delete(new DataManageInfo(
                    RecordID,null,null));
            return Response.success();
        }catch (Exception e){
            return Response.fail();
        }
    }
}
