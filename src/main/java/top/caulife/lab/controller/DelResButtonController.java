package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.AddResButtonService;

@RestController
public class DelResButtonController {
    @Autowired
    AddResButtonService addresbuttonService;

    @RequestMapping("/delresbuttonjs")
    public Response delresbuttonjs(@RequestParam("delunitexpstepno") int delunitexpstepno,
                                   @RequestParam("delunitno") int delunitno,
                                   @RequestParam("delrestype") String delrestype,
                                   @RequestParam("delresno") int delresno){
        addresbuttonService.delresbuttonjs(delunitexpstepno, delunitno, delrestype, delresno);
//        if (user!=null){
            return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}
