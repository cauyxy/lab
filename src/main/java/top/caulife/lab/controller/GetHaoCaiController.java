package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.YangPin;
import top.caulife.lab.service.GetHaoCaiService;

import java.util.List;

@RestController
public class GetHaoCaiController {
    @Autowired
    GetHaoCaiService gethaocaiService;

    @RequestMapping("/gethaocaijs")
    public Response gethaocaijs(){
        List<YangPin> gethaocai = gethaocaiService.gethaocaijs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success(gethaocai);
//        }
//        return Response.fail((Object) null);
    }

}
