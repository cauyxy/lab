package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.GetAddStepNo;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.GetAddStepNoService;

import java.util.List;

@RestController
public class GetAddStepNoController {
    @Autowired
    GetAddStepNoService getaddstepnoService;

    @RequestMapping("/getaddstepnojs")
    public Response getaddstepnojs(){
        List<GetAddStepNo> getaddstepno = getaddstepnoService.getaddstepnojs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
        return Response.success(getaddstepno);
//        }
//        return Response.fail((Object) null);
    }

}