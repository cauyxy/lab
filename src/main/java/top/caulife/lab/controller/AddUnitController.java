package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.AddUnitService;

@RestController
public class AddUnitController {
    @Autowired
    AddUnitService addunitService;

    @RequestMapping("/addunitjs")
    public Response addunitjs(@RequestParam("stepno") int stepno,
                              @RequestParam("unitno") int unitno,
                              @RequestParam("priority") int priority){
        addunitService.addunitjs(stepno, unitno, priority);
//        if (user!=null){
            return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}
