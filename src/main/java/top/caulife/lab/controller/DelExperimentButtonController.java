package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.AddExperimentButtonService;

@RestController
public class DelExperimentButtonController {
    @Autowired
    AddExperimentButtonService addexperimentbuttonService;

    @RequestMapping("/delexperimentbuttonjs")
    public Response delexperimentbuttonjs(@RequestParam("experimentno") int experimentno){
        try {
            addexperimentbuttonService.delexperimentbuttonjs(experimentno);
            return Response.success();
        } catch (Exception e) {
            System.out.println("AddexperimentbuttonService ERROR: " + e.getMessage());
            return Response.fail();
        }
    }

}
