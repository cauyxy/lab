package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.ExpBookingService;

import java.util.List;

@RestController
public class ChangeExStateController {
    @Autowired
    ExpBookingService changeexpstateService;

    @RequestMapping("/changeexpstatejs")
    public Response changeexpstatejs(@RequestParam("experimentno") int experimentno){
        changeexpstateService.changeExpStateById(experimentno, "10019990");
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}
