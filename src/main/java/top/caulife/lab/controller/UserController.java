package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.User;
import top.caulife.lab.entity.UserPermission;
import top.caulife.lab.entity.User_jys;
import top.caulife.lab.service.UserService;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping("/login")
    public Response login(@RequestParam("username") String username,
                          @RequestParam("password") String password){
        User user = userService.login(username,password);
        if (user!=null){
            return Response.success(user);
        }
        return Response.fail((Object) null);
    }

    @RequestMapping("/login_jys")
    public Response login_jys(@RequestParam("username") String username,
                              @RequestParam("password") String password){
        User_jys user = userService.login_jys(username,password);
        if (user!=null){
            return Response.success(user);
        }
        return Response.fail((Object) null);
    }

    @RequestMapping("/login_jys_permission")
    public Response login_jys_permission(
            @RequestParam("usertid") int usertid){
        UserPermission userPermission = userService.login_jys_permission(usertid);
        if (userPermission!=null){
            return Response.success(userPermission);
        }
        return Response.fail((Object) null);
    }

}
