package top.caulife.lab.controller;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.caulife.lab.entity.ExpFileInfo;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.ExpFileInfoService;
import top.caulife.lab.utils.FileUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/expFileInfo")
public class ExpFileInfoController {
    @Autowired
    ExpFileInfoService expFileInfoService;

    @RequestMapping("/getInfo")
    public Response getInfo(@RequestParam("id") String id) {
        ExpFileInfo expFileInfo = expFileInfoService.sel(id);
        if (expFileInfo != null) {
            return Response.success(expFileInfo);
        }
        return Response.fail("EXPFILEINFO FAIL");
    }

    @RequestMapping("/updateReport")
    public Response updateReport(@RequestParam("id") String id,
                                 @RequestParam("file") MultipartFile file) {
        String path_prefix = "./files/report/";
        if (!file.isEmpty()) {
            try {
                String file_id = UUID.randomUUID().toString();
                String file_name = file.getOriginalFilename();
                assert file_name != null;
                String path = path_prefix + file_id + file_name.substring(file_name.lastIndexOf('.'));
                file.transferTo(Paths.get(path));

                expFileInfoService.uploadReport(id, file_id, file_name, path);
            } catch (IOException e) {
                e.printStackTrace();
                return Response.fail("上传失败");
            }
            return Response.success("上传成功");
        } else {
            return Response.fail("文件是空");
        }
    }

    @RequestMapping("/updateData")
    public Response updateData(@RequestParam("id") String id,
                               @RequestParam("file") MultipartFile file) {

        String path_prefix = "./files/data/";
        if (!file.isEmpty()) {
            try {
                String file_id = UUID.randomUUID().toString();
                String file_name = file.getOriginalFilename();
                assert file_name != null;
                String path = path_prefix + file_id + file_name.substring(file_name.lastIndexOf('.'));
                file.transferTo(Paths.get(path));

                expFileInfoService.uploadData(id, file_id, file_name, path);
            } catch (IOException e) {
                e.printStackTrace();
                return Response.fail("上传失败");
            }
            return Response.success("上传成功");
        } else {
            return Response.fail("文件是空");
        }
    }

    @RequestMapping("/updateExtra")
    public Response updateExtra(@RequestParam("id") String id,
                                @RequestParam("file") MultipartFile file) {

        String path_prefix = "./files/extra/";
        if (!file.isEmpty()) {
            try {
                String file_id = UUID.randomUUID().toString();
                String file_name = file.getOriginalFilename();
                assert file_name != null;
                String path = path_prefix + file_id + file_name.substring(file_name.lastIndexOf('.'));
                file.transferTo(Paths.get(path));

                expFileInfoService.addExtraFile(id, file_id, file_name, path);
            } catch (IOException e) {
                e.printStackTrace();
                return Response.fail("上传失败");
            }
            return Response.success("上传成功");
        } else {
            return Response.fail("文件是空");
        }
    }

    @RequestMapping("/delReport")
    public Response delReport(@RequestParam("id") String id) {
        try {
            expFileInfoService.delReport(id);
            return Response.success(null);
        } catch (Exception e) {
            return Response.fail(null);
        }
    }

    @RequestMapping("/delData")
    public Response delData(@RequestParam("id") String id) {
        try {
            expFileInfoService.delData(id);
            return Response.success(null);
        } catch (Exception e) {
            return Response.fail(null);
        }
    }

    @RequestMapping("/delExtra")
    public Response delExtra(@RequestParam("id") String id,
                             @RequestParam("file_id") String file_id) {
        try {
            expFileInfoService.delExtraFile(id, file_id);
            return Response.success(null);
        } catch (Exception e) {
            return Response.fail(null);
        }
    }

    @RequestMapping("/download")
    public void download(HttpServletResponse httpServletResponse,
                         @RequestParam("path") String path,
                         @RequestParam("name") String name) throws IOException {
        FileUtil.download(path, httpServletResponse, name);
        System.out.println(path);
    }

    @RequestMapping("/downloadAll")
    public void downloadAll(HttpServletResponse httpServletResponse,
                         @RequestParam("name") String name,
                         @RequestParam("file_list") String file_list) throws IOException, JSONException {
        System.out.println(name);
        System.out.println(file_list);

        List<String> path_lis = new ArrayList<>();
        List<String> name_lis = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(file_list);

        for (int i = 0; i < jsonArray.length(); i++) {
            path_lis.add(jsonArray.getJSONObject(i).getString("path"));
            name_lis.add(jsonArray.getJSONObject(i).getString("name"));
        }

        FileUtil.zipFiles(httpServletResponse,name_lis, path_lis,name);
    }
}
