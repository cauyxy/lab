package top.caulife.lab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.PIInfo;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.PIInfoService;

import java.util.List;

@RestController
@RequestMapping("/piInfo")
public class PIInfoController {
    @Autowired
    PIInfoService piInfoService;

    @RequestMapping("/getInfo")
    public Response getInfo(@RequestParam("pi_id") String pi_id){
        List<PIInfo> piInfo = piInfoService.sel(pi_id);
        if (piInfo != null) {
            return Response.success(piInfo);
        }
        return Response.fail("PIInfo FAIL");
    }
}
