package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.ExpUpdBookService;


@RestController
public class ExpUpdBookController {
    @Autowired
    ExpUpdBookService expUpdBookService;

    @RequestMapping("/expbookupd/upd_exp_mes")
    public Response updExpMessageById(
            @RequestParam("exp_id") int exp_id,
            @RequestParam("exp_name") String exp_name,
            @RequestParam("pi_id") int pi_id,
            @RequestParam("as_id") int as_id){
        if (expUpdBookService.updExpMessageById(exp_id, exp_name, pi_id, as_id)){
            return Response.success();
        } else {
            return Response.fail();
        }
    }

}
