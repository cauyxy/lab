package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.AddExperimentButtonService;

@RestController
public class AddExperimentButtonController {
    @Autowired
    AddExperimentButtonService addexperimentbuttonService;

    @RequestMapping("/addexperimentbuttonjs")
    public Response addexperimentbuttonjs(@RequestParam("projectno") String projectno,
                                          @RequestParam("projectname") String projectname,
                                          @RequestParam("experimentno") int experimentno,
                                          @RequestParam("experimentname") String experimentname,
                                          @RequestParam("PIno") int PIno,
                                          @RequestParam("assistantno") int assistantno){
        addexperimentbuttonService.addexperimentbuttonjs(projectno, projectname, experimentno, experimentname, PIno, assistantno);
//        if (user!=null){
            return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}
