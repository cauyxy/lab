package top.caulife.lab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.caulife.lab.entity.ProPartialInfo;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.ProPartialInfoService;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import java.util.List;

@RestController
@RequestMapping("/proPartialInfo")
public class ProPartialInfoController {
    @Autowired
    ProPartialInfoService proPartialInfoService;

    @RequestMapping("/getInfo")
    public Response getInfo(@RequestParam("exp_id") String exp_id, @RequestParam("pro_id") String pro_id, @RequestParam("pro_name") String pro_name, @RequestParam("pro_state") String pro_state) {
        List<ProPartialInfo> proPartialInfo = proPartialInfoService.sel(exp_id, pro_id, pro_name, pro_state);
        if (!proPartialInfo.isEmpty()) {
            return Response.success(proPartialInfo);
        }
        return Response.fail("PROPARTIALINFO FAIL");
    }

    @RequestMapping("/judge_authorization")
    public Response judge_authorization(@RequestParam("exp_id") String exp_id, @RequestParam("pro_id") String pro_id, @RequestParam("pi_id") String pi_id,
                                        @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss") Date cur_time) {
        List<ProPartialInfo> proPartialInfo = proPartialInfoService.judge_authorization(exp_id, pro_id, pi_id, cur_time);
        if (!proPartialInfo.isEmpty()) {
            return Response.success(proPartialInfo);
        }
        return Response.fail("AUTHORIZATION FAIL");
    }

    @RequestMapping("/updateState")
    public Response updateState(@RequestParam("exp_id") String exp_id, @RequestParam("pro_id") String pro_id, @RequestParam("pro_state") String pro_state, @RequestParam("other_info") String other_info) {
        int matched_num = proPartialInfoService.updateState(exp_id, pro_id, pro_state, other_info);
        System.out.println(matched_num);
        if (matched_num != 0)
            return Response.success("STATEUPDATE SUCCESS");
        else return Response.fail("STATEUPDATE Fail");
    }
    @RequestMapping("/allDelete")
    public Response allDelete(@RequestParam("exp_id") String exp_id,
                                @RequestParam("cur_time")  @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss") Date cur_time,
                                @RequestParam("pro_state") String pro_state, @RequestParam("other_info") String other_info) {
        int matched_num = proPartialInfoService.allDelete(exp_id, cur_time, pro_state, other_info);
        System.out.println(matched_num);
        if (matched_num != 0)
            return Response.success(matched_num);
        else return Response.fail("STATEUPDATE Fail");
    }

    @RequestMapping("/updateCheckLog")
    public Response updateCheckLog(@RequestParam("pro_id") String pro_id,@RequestParam("checklog_id") String checklog_id) {
        int matched_num = proPartialInfoService.updateCheckLog(pro_id, checklog_id);
        System.out.println(matched_num);
        if (matched_num != 0)
            return Response.success("updateCheckLog SUCCESS");
        else return Response.fail("updateCheckLog Fail");
    }

}
