package top.caulife.lab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.CheckLog;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.CheckLogService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/checkLog")
public class CheckLogController {
    @Autowired
    CheckLogService checkLogService;

    @RequestMapping("/getInfo")
    public Response getInfo(@RequestParam("checklog_id") String checklog_id){
        List<CheckLog> checkLog = checkLogService.sel(checklog_id);
        if (checkLog != null) {
            return Response.success(checkLog);
        }
        return Response.fail("CheckLog FAIL");
    }

    @RequestMapping("/record")
    public Response record( @RequestParam("checklog_id") String checklog_id,
                            @RequestParam("checker_id") String checker_id,
                            @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
                            @RequestParam("check_time") Date check_time,
                            @RequestParam("check_result") String check_result){
        checkLogService.record( checklog_id, checker_id, check_time, check_result);
//        int inserted_num = loginLabLogService.record(uid, username, role, loginlab_time);
//        if (inserted_num == 1){
        return Response.success("CHECKLOG SUCCESS");
//        }
//        return Response.fail("LOGINLABLOG FAIL");
    }
}
