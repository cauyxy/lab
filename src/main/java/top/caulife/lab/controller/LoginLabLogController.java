package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.LoginLabLog;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.LoginLabLogService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/loginLabLog")

public class LoginLabLogController {
    @Autowired
    LoginLabLogService loginLabLogService;

    @RequestMapping("/record")
    public Response record( @RequestParam("uid") int uid,
                            @RequestParam("username") String username,
                            @RequestParam("role") String role,
                            @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
                            @RequestParam("loginlab_time") Date loginlab_time){
        loginLabLogService.record(uid, username, role, loginlab_time);
//        int inserted_num = loginLabLogService.record(uid, username, role, loginlab_time);
//        if (inserted_num == 1){
            return Response.success("LOGINLABLOG SUCCESS");
//        }
//        return Response.fail("LOGINLABLOG FAIL");
    }

    @RequestMapping("/getInfo")
    public Response getInfo(@RequestParam("username") String username,@RequestParam("role") String role,
                            @RequestParam("start_time") String start_time,@RequestParam("end_time") String end_time){
        List<LoginLabLog> loginLabLog = loginLabLogService.sel(username,role,start_time,end_time);
        if (loginLabLog != null) {
            return Response.success(loginLabLog);
        }
        return Response.fail("LoginLabLog FAIL");
    }

    @RequestMapping("/deleteInfo")
    public Response del(@RequestParam("loginlab_id") int loginlab_id){
        loginLabLogService.del(loginlab_id);
        return Response.success("LOGINLABLOG DELETE SUCCESS");
    }
}
