package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.bookingMessage.SubmitUnitTime;
import top.caulife.lab.service.ExpSubmitService;

import java.util.List;


@RestController
public class ExpSubmitController {
    @Autowired
    ExpSubmitService expSubmitService;

    @RequestMapping("/expbookupd/submit_unit_time")
    public Response expSubmit(@RequestBody List<SubmitUnitTime> uts){
        if(expSubmitService.expSubmit(uts)) {
            return Response.success();
        } else {
            return Response.fail();
        }
    }
}
