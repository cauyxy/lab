package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.AddStepButtonService;

@RestController
public class DelStepButtonController {
    @Autowired
    AddStepButtonService addstepbuttonService;

    @RequestMapping("/delstepbuttonjs")
    public Response delstepbuttonjs(@RequestParam("delsteono") int delsteono){
        addstepbuttonService.delstepbuttonjs(delsteono);
//        if (user!=null){
            return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}
