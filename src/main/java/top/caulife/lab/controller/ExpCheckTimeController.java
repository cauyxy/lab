package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.bookingMessage.ResMessage;
import top.caulife.lab.entity.bookingMessage.UnitTime;
import top.caulife.lab.entity.bookingTime.QueryTime;
import top.caulife.lab.entity.bookingTime.TimeSlot;
import top.caulife.lab.service.BookingService;
import top.caulife.lab.service.ExpUpdBookService;

import java.util.List;


@RestController
public class ExpCheckTimeController {
    @Autowired
    ExpUpdBookService expUpdBookService;
    @Autowired
    BookingService bookingService;

    @RequestMapping("/expbookupd/query_unit_time")
    public Response updExpMessageById(
            @RequestParam("exp_id") int exp_id,
            @RequestParam("step_id") int step_id,
            @RequestParam("step_pr") int step_pr,
            @RequestParam("unit_id") int unit_id,
            @RequestParam("unit_pr") int unit_pr){
        TimeSlot qts = bookingService.queryTime(exp_id, step_id, step_pr, unit_id, unit_pr);
        return Response.success(qts.tS);
    }

    @RequestMapping("/expbooking/get_unit_time")
    public Response getUnitTime(
            @RequestParam("exp_id") int exp_id,
            @RequestParam("step_id") int step_id,
            @RequestParam("step_pr") int step_pr,
            @RequestParam("unit_id") int unit_id,
            @RequestParam("unit_pr") int unit_pr){
        UnitTime uts = expUpdBookService.getUnitTime(exp_id, step_id, step_pr, unit_id, unit_pr);
        uts.setUnit_id_str();
        return Response.success(uts);
    }

    @RequestMapping("/expbooking/set_unit_time")
    public Response setUnitTime(
            @RequestParam("exp_id") int exp_id,
            @RequestParam("step_id") int step_id,
            @RequestParam("step_pr") int step_pr,
            @RequestParam("unit_id") int unit_id,
            @RequestParam("unit_pr") int unit_pr,
            @RequestParam("unit_hour") int unit_hour){
        if (expUpdBookService.setUnitTime(exp_id, step_id, step_pr, unit_id, unit_pr, unit_hour)) {
            return Response.success();
        }
        return Response.fail();
    }
}
