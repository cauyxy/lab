package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.AddResButtonService;

@RestController
public class AddResButtonController {
    @Autowired
    AddResButtonService addresbuttonService;

    @RequestMapping("/addresbuttonjs")
    public Response addresbuttonjs(@RequestParam("unitexpstepno") int unitexpstepno,
                                          @RequestParam("unitno") int unitno,
                                          @RequestParam("type") String type,
                                          @RequestParam("resno") int resno,
                                          @RequestParam("resnum") int resnum){
        addresbuttonService.addresbuttonjs(unitexpstepno, unitno, type, resno, resnum);
//        if (user!=null){
            return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}
