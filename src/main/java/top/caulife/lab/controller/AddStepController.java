package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;

import top.caulife.lab.entity.AddStep;
import top.caulife.lab.service.AddStepService;

@RestController
public class AddStepController {
    @Autowired
    AddStepService addstepService;

    @RequestMapping("/addstepjs")
    public Response addstepjs(@RequestParam("stepno") int stepno,
                              @RequestParam("stepname") String stepname,
                              @RequestParam("stepmess") String stepmess){
        addstepService.addstepjs(stepno, stepname, stepmess);
//        if (user!=null){
            return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}
