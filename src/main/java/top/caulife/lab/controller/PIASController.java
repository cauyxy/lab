package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.PIAS;
import top.caulife.lab.service.PIASService;

import java.util.List;

@RestController
public class PIASController {
    @Autowired
    PIASService piasService;

    @RequestMapping("/piasjs")
    public Response piasjs(){
        List<PIAS> pias = piasService.piasjs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success(pias);
//        }
//        return Response.fail((Object) null);
    }

}
