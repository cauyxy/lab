package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.YangPin;
import top.caulife.lab.service.GetYiQiService;

import java.util.List;

@RestController
public class GetYiQiController {
    @Autowired
    GetYiQiService getyiqiService;

    @RequestMapping("/getyiqijs")
    public Response getyiqijs(){
        List<YangPin> getyiqi = getyiqiService.getyiqijs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success(getyiqi);
//        }
//        return Response.fail((Object) null);
    }

}
