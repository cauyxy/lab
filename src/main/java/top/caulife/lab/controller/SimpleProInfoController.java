package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.SimpleProInfo;
import top.caulife.lab.service.SimpleProInfoService;

import java.util.List;

@RestController
@RequestMapping("/simpleProInfo")
public class SimpleProInfoController {
    @Autowired
    SimpleProInfoService simpleProInfoService;

    @RequestMapping("/getInfo")
    public Response getInfo(@RequestParam("pi_id") String pi_id){
        List<SimpleProInfoService.ProjectInfo> simpleProInfos = simpleProInfoService.sel(pi_id);
        if (simpleProInfos!=null){
            return Response.success(simpleProInfos);
        }
        return Response.fail();
    }
}
