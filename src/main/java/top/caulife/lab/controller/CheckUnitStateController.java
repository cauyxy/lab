package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;

import top.caulife.lab.entity.CheckUnitState;
import top.caulife.lab.service.CheckUnitStateService;

@RestController
public class CheckUnitStateController {
    @Autowired
    CheckUnitStateService checkunitstateService;

    @RequestMapping("/checkunitstatejs")
    public Response checkunitstatejs(@RequestParam("unitno") String unitno){
        CheckUnitState checkunitstate = checkunitstateService.checkunitstatejs(unitno);
//        if (user!=null){
            return Response.success(checkunitstate);
//        }
//        return Response.fail((Object) null);
    }

}
