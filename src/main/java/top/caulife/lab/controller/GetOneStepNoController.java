package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.GetOneStepNo;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.GetOneStepNoService;

import java.util.List;

@RestController
public class GetOneStepNoController {
    @Autowired
    GetOneStepNoService getonestepnoService;

    @RequestMapping("/getonestepnojs")
    public Response getonestepnojs(){
        List<GetOneStepNo> getonestepno = getonestepnoService.getonestepnojs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
        return Response.success(getonestepno);
//        }
//        return Response.fail((Object) null);
    }

}