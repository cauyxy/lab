package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.bookingMessage.ExpMessage;
import top.caulife.lab.entity.bookingMessage.ResMessage;
import top.caulife.lab.entity.bookingMessage.UnitMessage;
import top.caulife.lab.entity.bookingTime.StepIden;
import top.caulife.lab.service.ExpBookingService;
import top.caulife.lab.service.ProPartialInfoService;

import java.util.Date;
import java.util.List;

@RestController
public class ExpBookingController {
    @Autowired
    ExpBookingService expBookingService;
    @Autowired
    ProPartialInfoService proPartialInfoService;

    @RequestMapping("/expbooking/get_exp_mes")
    public Response getExpMessageById(
            @RequestParam("uid") int uid,
            @RequestParam("utypeid") int utypeid){
        List<ExpMessage> expMessage = expBookingService.getExpMessage(uid, utypeid);
        return Response.success(expMessage);
    }

    @RequestMapping("/expbooking/get_step_iden")
    public Response getStepIdenById(
            @RequestParam("exp_id") int experiment_id){
        List<StepIden> expMessage = expBookingService.getStepIdenById(experiment_id);
        return Response.success(expMessage);
    }

    @RequestMapping("/expbooking/get_unit_iden_mes")
    public Response getUnitIdenMessage(
            @RequestParam("exp_id") int experiment_id,
            @RequestParam("step_id") int step_id,
            @RequestParam("step_pr") int step_priority){
        List<UnitMessage> expMessage = expBookingService.getUnitIdenMessage(experiment_id, step_id, step_priority);
        return Response.success(expMessage);
    }

    @RequestMapping("/expbooking/get_unit_static_res")
    public Response getUnitStaticMaterialMessage(
            @RequestParam("unit_id") int unit_id){
        List<ResMessage> expMessage = expBookingService.getUnitStaticMaterialMessage(unit_id);
        return Response.success(expMessage);
    }

    @RequestMapping("/expbooking/get_unit_all_res")
    public Response getUnitStaticMaterialMessage(
            @RequestParam("exp_id") int exp_id,
            @RequestParam("step_id") int step_id,
            @RequestParam("step_pr") int step_priority,
            @RequestParam("unit_id") int unit_id){
        List<ResMessage> expMessage = expBookingService.getUnitAllMaterialMessage(exp_id, step_id, step_priority, unit_id);
        return Response.success(expMessage);
    }

    @RequestMapping("/expbooking/del_exp")
    public Response delExpById(
            @RequestParam("exp_id") int experiment_id){
        if (expBookingService.delExpById(experiment_id))
            return Response.success();
        else
            return Response.fail();
    }

    @RequestMapping("/expbooking/drop_exp")
    public Response dropExpById(
            @RequestParam("exp_id") int experiment_id){
        proPartialInfoService.allDelete(
                String.format("%d", experiment_id), new Date(),
                "暂被撤销", "手动撤销"
        );
        if (expBookingService.changeExpStateById(experiment_id, "39990013"))
            return Response.success();
        else
            return Response.fail();
    }

    @RequestMapping("/expbooking/change_exp_state")
    public Response changeExpStateById(
            @RequestParam("exp_id") int experiment_id,
            @RequestParam("pgr_id") String progress_id){
        if (expBookingService.changeExpStateById(experiment_id, progress_id))
            return Response.success();
        else
            return Response.fail();
    }

}