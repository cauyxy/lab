package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.AddStepButtonService;

@RestController
public class AddStepButtonController {
    @Autowired
    AddStepButtonService addstepbuttonService;

    @RequestMapping("/addstepbuttonjs")
    public Response addstepbuttonjs(@RequestParam("experimentno") int experimentno,
                                    @RequestParam("stepno") int stepno,
                                    @RequestParam("steppriority") int steppriority,
                                    @RequestParam("steptime") int steptime){
        addstepbuttonService.addstepbuttonjs(experimentno, stepno, steppriority, steptime);
//        if (user!=null){
        return Response.success();
//        }
//        return Response.fail((Object) null);
    }

}