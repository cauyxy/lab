package top.caulife.lab.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.caulife.lab.entity.StepNoName;
import top.caulife.lab.entity.Response;
import top.caulife.lab.service.StepNoNameService;

import java.util.List;

@RestController
public class StepNoNameController {
    @Autowired
    StepNoNameService stepnonameService;

    @RequestMapping("/stepnonamejs")
    public Response stepnonamejs(){
        List<StepNoName> stepnoname = stepnonameService.stepnonamejs();
//        CheckStepState checkstepstate = checkstepstateService.checkstepstatejs(stepno);
//        if (user!=null){
            return Response.success(stepnoname);
//        }
//        return Response.fail((Object) null);
    }

}
