package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.YangPin;
import top.caulife.lab.mapper.GetYangPinMapper;

import java.util.List;

@Service
public class GetYangPinService {
    @Autowired
    GetYangPinMapper getyangpinMapper;

    public List<YangPin> getyangpinjs(){
        return getyangpinMapper.getyangpinjs();
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}
