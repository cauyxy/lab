package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.CheckStepState;
import top.caulife.lab.mapper.CheckStepStateMapper;

import java.util.List;

@Service
public class CheckStepStateService {
    @Autowired
    CheckStepStateMapper checkstepstateMapper;

    public List<CheckStepState> checkstepstatejs(int stepno){
        return checkstepstateMapper.checkstepstatejs(stepno);
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}
