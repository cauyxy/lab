package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.GetAddUnitNo;
import top.caulife.lab.mapper.GetAddUnitNoMapper;

import java.util.List;

@Service
public class GetAddUnitNoService {
    @Autowired
    GetAddUnitNoMapper getaddunitnoMapper;

    public List<GetAddUnitNo> getaddunitnojs(){
        return getaddunitnoMapper.getaddunitnojs();
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}