package top.caulife.lab.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.ExpFileInfo;
import top.caulife.lab.mapper.ExpFileInfoMapper;

@Service
public class ExpFileInfoService {
    @Autowired
    ExpFileInfoMapper expFileInfoMapper;

    public ExpFileInfo sel(String id) {
        ExpFileInfo expFileInfo = expFileInfoMapper.sel(id);
        expFileInfo.setPi_name(expFileInfoMapper.getPiName(expFileInfo.getPi_id()));
        return expFileInfo;
    }

    public void uploadReport(String id, String file_id, String filename, String path){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",file_id);
            jsonObject.put("name",filename);
            jsonObject.put("path",path);

            expFileInfoMapper.updateReport(id,jsonObject.toString());
        } catch (JSONException ignored){
        }
    }

    public void delReport(String id){
        try{
            expFileInfoMapper.updateReport(id, null);
        }catch (Exception ignored){
        }
    }

    public void uploadData(String id, String file_id, String filename, String path){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",file_id);
            jsonObject.put("name",filename);
            jsonObject.put("path",path);

            expFileInfoMapper.updateData(id,jsonObject.toString());
        } catch (JSONException ignored){
        }
    }

    public void delData(String id){
        try{
            expFileInfoMapper.updateData(id,null);
        }catch (Exception ignored){
        }
    }

    public void addExtraFile(String id, String file_id, String filename, String path){
        try{
            ExpFileInfo expFileInfo = expFileInfoMapper.sel(id);
            String file_extra = expFileInfo.getFile_extra();
            JSONArray jsonArray = new JSONArray(file_extra);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",file_id);
            jsonObject.put("name",filename);
            jsonObject.put("path",path);
            jsonArray.put(jsonObject);

            expFileInfoMapper.updateExtra(id,jsonArray.toString());
        } catch (JSONException ignored){
        }
    }

    public void delExtraFile(String id, String file_id){
        try{
            ExpFileInfo expFileInfo = expFileInfoMapper.sel(id);
            String file_extra = expFileInfo.getFile_extra();
            JSONArray jsonArray = new JSONArray(file_extra);
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).getString("id").equals(file_id)){
                    jsonArray.remove(i);
                    break;
                }
            }

            expFileInfoMapper.updateExtra(id,jsonArray.toString());
        } catch (JSONException ignored){
        }
    }
}
