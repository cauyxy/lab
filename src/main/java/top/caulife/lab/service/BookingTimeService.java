package top.caulife.lab.service;

import org.springframework.stereotype.Service;
import top.caulife.lab.entity.bookingTime.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class BookingTimeService {

    Random random = new Random();


    public void deleteTime(OrderTime orderTime) { }


    public OrderTimeReply orderTime(OrderTime orderTime) {
        return new OrderTimeReply(orderTime.unit_id_str,
                orderTime.resource_type,
                orderTime.resource_id,
                true);
    }


    public QueryTimeReply queryTime(QueryTime queryTime) {

        return new QueryTimeReply(
                queryTime.unit_id_str,
                queryTime.resource_type,
                queryTime.resource_id,
                newTimeSlot(
                        60L * 60 * (randomInt())*1000*12,
                        60L * 60 * (randomInt()+15)*1000*12)
        );
    }


    public ResourceListReply resourceList(ResourceList resourceList) {

        List<Resource> resources = new ArrayList<>();
        for (int i = 0; i < 2 + randomInt(); i++) {
            String resourceName = resourceList.resource_type.toString() + ": 资源" + (char) ('A' + i);
            resources.add(
                    new Resource(
                            i,
                            resourceName,
                            "这是" + resourceName));
        }

        return new ResourceListReply(
                resourceList.resource_type,
                resources
        );
    }

    public static TimeSlot newTimeSlot(long begin, long period) {
        long now = new Date().getTime()+8*60*60*1000;
        return new TimeSlot(
                new TimeSlot.Stage(
                        new Date(now + begin),
                        new Date(now + begin + period)));
    }

    public int randomInt() {
        return random.nextInt(20);
    }
}
