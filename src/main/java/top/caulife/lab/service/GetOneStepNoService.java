package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.GetOneStepNo;
import top.caulife.lab.mapper.GetOneStepNoMapper;

import java.util.List;

@Service
public class GetOneStepNoService {
    @Autowired
    GetOneStepNoMapper getonestepnoMapper;

    public List<GetOneStepNo> getonestepnojs(){
        return getonestepnoMapper.getonestepnojs();
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}