package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.mapper.AddStepButtonMapper;

@Service
public class AddStepButtonService {
    @Autowired
    AddStepButtonMapper addstepbuttonMapper;

    public void addstepbuttonjs(int experimentno, int stepno, int steppriority, int steptime){
        addstepbuttonMapper.addstepbuttonjs(experimentno, stepno, steppriority, steptime);
    }

    public void delstepbuttonjs(int delsteono){
        addstepbuttonMapper.delstepbuttonjs(delsteono);
    }
}
