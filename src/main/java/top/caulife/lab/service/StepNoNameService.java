package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.ExperimentStep;
import top.caulife.lab.entity.StepNoName;
import top.caulife.lab.entity.ExperimentStep;
import top.caulife.lab.mapper.StepNoNameMapper;

import java.util.List;

@Service
public class StepNoNameService {
    @Autowired
    StepNoNameMapper stepnonameMapper;

    public List<StepNoName> stepnonamejs(){
        return stepnonameMapper.stepnonamejs();
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}
