package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.DataManageInfo;
import top.caulife.lab.mapper.DataManageInfoMapper;

import java.util.List;

@Service
public class DataManageInfoService {
    @Autowired
    DataManageInfoMapper dataManageInfoMapper;

    public List<DataManageInfo> sel(){
        return dataManageInfoMapper.sel();
    }

    public void insert(DataManageInfo dataManageInfo){
        dataManageInfoMapper.insert(dataManageInfo);
    }

    public void delete(DataManageInfo dataManageInfo){
        dataManageInfoMapper.delete(dataManageInfo);
    }
}
