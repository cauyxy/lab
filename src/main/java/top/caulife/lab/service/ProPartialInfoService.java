package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.ProPartialInfo;
import top.caulife.lab.mapper.ProPartialInfoMapper;
import top.caulife.lab.service.ExpBookingService;
import java.util.List;
import java.util.Date;

@Service
public class ProPartialInfoService {
    @Autowired
    ProPartialInfoMapper proPartialInfoMapper;

    public List<ProPartialInfo> sel(String exp_id, String pro_id, String pro_name, String pro_state){
        return proPartialInfoMapper.sel(exp_id, pro_id, pro_name, pro_state);
    }

    public List<ProPartialInfo> judge_authorization(String exp_id, String pro_id, String pi_id,
                                                    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss") Date cur_time){
        return proPartialInfoMapper.judge_authorization(exp_id, pro_id, pi_id, cur_time);
    }

    public int updateState(String exp_id, String pro_id, String pro_state, String other_info){
        ExpBookingService expBookingService = new ExpBookingService();
        if(pro_state == "暂被撤销")
            expBookingService.changeExpStateById(exp_id, "39990013");
        return proPartialInfoMapper.updateState(exp_id, pro_id, pro_state, other_info);
    }

    public int allDelete(String exp_id, @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss") Date cur_time,
                         String pro_state, String other_info){
        return proPartialInfoMapper.allDelete(exp_id, cur_time, pro_state, other_info);
    }
    public int updateCheckLog(String pro_id,String checklog_id){
        return proPartialInfoMapper.updateCheckLog(pro_id, checklog_id);
    }
}
