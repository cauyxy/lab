package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.FuncUnit;
import top.caulife.lab.mapper.FuncUnitMapper;

@Service
public class FuncUnitService{
    @Autowired
    FuncUnitMapper funcUnitMapper;

    public FuncUnit sel(String id){
        return funcUnitMapper.sel(id);
    }

    public void  updateState(String id, String state){
        funcUnitMapper.updateState(id, state);
    }

    public void updateResInfo(String id,
                              String animal_info,
                              String equ_info,
                              String drug_info,
                              String sample_info){
        funcUnitMapper.updateAnimalInfo(id, animal_info);
        funcUnitMapper.updateEquInfo(id,equ_info);
        funcUnitMapper.updateDrugInfo(id,drug_info);
        funcUnitMapper.updateSampleInfo(id,sample_info);
    }
}
