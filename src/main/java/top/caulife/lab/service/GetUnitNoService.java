package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.GetUnitNo;
import top.caulife.lab.mapper.GetUnitNoMapper;

import java.util.List;

@Service
public class GetUnitNoService {
    @Autowired
    GetUnitNoMapper getunitnoMapper;

    public List<GetUnitNo> getunitnojs(String unitno){
        return getunitnoMapper.getunitnojs(unitno);
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}