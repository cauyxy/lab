package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.GetAddStepNo;
import top.caulife.lab.mapper.GetAddStepNoMapper;

import java.util.List;

@Service
public class GetAddStepNoService {
    @Autowired
    GetAddStepNoMapper getaddstepnoMapper;

    public List<GetAddStepNo> getaddstepnojs(){
        return getaddstepnoMapper.getaddstepnojs();
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}