package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.YangPin;
import top.caulife.lab.mapper.GetHaoCaiMapper;

import java.util.List;

@Service
public class GetHaoCaiService {
    @Autowired
    GetHaoCaiMapper gethaocaiMapper;

    public List<YangPin> gethaocaijs(){
        return gethaocaiMapper.gethaocaijs();
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}
