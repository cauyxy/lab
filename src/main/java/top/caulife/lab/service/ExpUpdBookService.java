package top.caulife.lab.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.Response;
import top.caulife.lab.entity.bookingMessage.UnitTime;
import top.caulife.lab.entity.bookingTime.StepIden;
import top.caulife.lab.mapper.UpdExpMapper;

import java.util.List;


@Service
public class ExpUpdBookService {
    @Autowired
    UpdExpMapper updExpMapper;

    public boolean updExpMessageById(int exp_id, String exp_name, int pi_id, int as_id){
        try {
            updExpMapper.updExpMessageById(exp_id, exp_name, pi_id, as_id);
            return true;
        } catch (Exception e) {
            System.out.println("ExpUpdBookService ERROR: " + e.getMessage());
            return false;
        }
    }

    public UnitTime getUnitTime(int exp_id, int step_id, int step_pr, int unit_id, int unit_pr){
        UnitTime uts = updExpMapper.getUnitTime(exp_id, step_id, step_pr, unit_id, unit_pr);
        if (uts == null) {
            updExpMapper.addUnitTime(updExpMapper.selUnitTime(exp_id, step_id, step_pr).getExp_step_id(), unit_id, unit_pr);
            return updExpMapper.getUnitTime(exp_id, step_id, step_pr, unit_id, unit_pr);
        } else {
            return uts;
        }
    }

    public boolean setUnitTime(int exp_id, int step_id, int step_pr, int unit_id, int unit_pr, int unit_hour){
        try {
            updExpMapper.updUnitTime(updExpMapper.selUnitTime(exp_id, step_id, step_pr).getExp_step_id(), unit_id, unit_pr, unit_hour);
            return true;
        } catch (Exception e) {
            System.out.println("ExpUpdBookService ERROR: " + e.getMessage());
            return false;
        }
    }
}
