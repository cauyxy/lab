package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.User;
import top.caulife.lab.entity.UserPermission;
import top.caulife.lab.entity.User_jys;
import top.caulife.lab.mapper.UserMapper;

@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    public User login(String username,String password){
        return userMapper.login(username,password);
    }

    public User_jys login_jys(String username, String password){
        return userMapper.login_jys(username,password);
    }

    public UserPermission login_jys_permission(int usertid){
        return userMapper.login_jys_permission(usertid);
    }
}
