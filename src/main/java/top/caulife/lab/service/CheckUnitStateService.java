package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.caulife.lab.entity.CheckUnitState;
import top.caulife.lab.mapper.CheckUnitStateMapper;

@Service
public class CheckUnitStateService {
    @Autowired
    CheckUnitStateMapper checkunitstateMapper;

    public CheckUnitState checkunitstatejs(String unitno){
        return checkunitstateMapper.checkunitstatejs(unitno);
    }
}
