package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.ExperimentStep;
import top.caulife.lab.mapper.ExperimentStepButtonMapper;

import java.util.List;

@Service
public class ExperimentStepButtonService {
    @Autowired
    ExperimentStepButtonMapper experimentstepbuttonMapper;

    public List<ExperimentStep> getexperimentstepjs(int experimentno){
        return experimentstepbuttonMapper.getexperimentstepjs(experimentno);
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}
