package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.PIAS;
import top.caulife.lab.mapper.PIASMapper;

import java.util.List;

@Service
public class PIASService {
    @Autowired
    PIASMapper piasMapper;

    public List<PIAS> piasjs(){
        return piasMapper.piasjs();
    }
//    public CheckStepState checkstepstatejs(int stepno){
//        return checkstepstateMapper.checkstepstatejs(stepno);
//    }
}
