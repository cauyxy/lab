package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.mapper.AddResButtonMapper;

@Service
public class AddResButtonService {
    @Autowired
    AddResButtonMapper addresbuttonMapper;

    public void addresbuttonjs(int unitexpstepno, int unitno, String type, int resno, int resnum){
        addresbuttonMapper.addresbuttonjs(unitexpstepno, unitno, type, resno, resnum);
    }

    public void delresbuttonjs(int delunitexpstepno, int delunitno, String delrestype, int delresno){
        addresbuttonMapper.delresbuttonjs(delunitexpstepno, delunitno, delrestype, delresno);
    }
}
