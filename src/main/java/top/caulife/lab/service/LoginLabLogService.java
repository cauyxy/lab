package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.LoginLabLog;
import top.caulife.lab.mapper.LoginLabLogMapper;


import java.util.Date;
import java.util.List;

@Service
public class LoginLabLogService {
    @Autowired
    LoginLabLogMapper loginLabLogMapper;
    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    public void record(int uid, String username,String role, Date loginlab_time){
        loginLabLogMapper.record(uid, username, role, loginlab_time);
    }
    public List<LoginLabLog> sel(String username, String role, String start_time, String end_time){
        return loginLabLogMapper.sel(username, role, start_time, end_time);
    }

    public void del(int loginlab_id){
        loginLabLogMapper.del(loginlab_id);
    }
}
