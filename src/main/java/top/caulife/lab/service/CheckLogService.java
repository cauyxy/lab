package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.CheckLog;
import top.caulife.lab.mapper.CheckLogMapper;

import java.util.Date;
import java.util.List;

@Service
public class CheckLogService {
    @Autowired
    CheckLogMapper checkLogMapper;

    public List<CheckLog> sel(String checklog_id) {
        return checkLogMapper.sel(checklog_id);
    }

    @DateTimeFormat(pattern ="yyyy-MM-dd HH:mm:ss")
    public void record(String checklog_id, String checker_id,Date check_time, String check_result){
        checkLogMapper.record(checklog_id, checker_id, check_time, check_result);
    }

}
