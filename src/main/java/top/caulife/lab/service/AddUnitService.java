package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.AddUnit;
import top.caulife.lab.mapper.AddUnitMapper;

@Service
public class AddUnitService {
    @Autowired
    AddUnitMapper addunitMapper;

    public void addunitjs(int stepno, int unitno, int priority){
        addunitMapper.addunitjs(stepno, unitno, priority);
    }
}
