package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.PIInfo;
import top.caulife.lab.mapper.PIInfoMapper;

import java.util.List;

@Service
public class PIInfoService {
    @Autowired
    PIInfoMapper piInfoMapper;

    public List<PIInfo> sel( String pi_id) {
        return piInfoMapper.sel(pi_id);
    }

}
