package top.caulife.lab.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.SimpleProInfo;
import top.caulife.lab.entity.bookingMessage.ExpInfo;
import top.caulife.lab.entity.bookingMessage.ResMessage;
import top.caulife.lab.entity.bookingMessage.SubmitUnitTime;
import top.caulife.lab.entity.bookingMessage.UnitTime;
import top.caulife.lab.entity.bookingTime.*;
import top.caulife.lab.mapper.ExpInfoMapper;
import top.caulife.lab.mapper.ExpMesMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ExpSubmitService {
    @Autowired
    ExpMesMapper expMesMapper;
    @Autowired
    ExpBookingService expBookingService;
    @Autowired
    ExpUpdBookService expUpdBookService;
    @Autowired
    BookingTimeService bookingTimeService;
    @Autowired
    SimpleProInfoService simpleProInfoService;
    @Autowired
    ExpInfoMapper expInfoMapper;

    public boolean expSubmit(List<SubmitUnitTime> uts){
        try{
            if (uts.size()>0){
                expBookingService.changeExpStateById(uts.get(0).getExp_id(), "10049990");
            }
            for (SubmitUnitTime ut : uts) {
                int exp_id = ut.getExp_id();
                int step_id = ut.getStep_id();
                int step_pr = ut.getStep_pr();
                int unit_id = ut.getUnit_id();
                String unit_id_str = ut.getUnit_id_str();
                int unit_pr = ut.getUnit_pr();
                String unit_time_str = ut.getUnit_time();
                Date unit_time = ut.getUnit_date();

                List<ResMessage> rms = expBookingService.getUnitAllMaterialMessage(exp_id, step_id, step_pr, unit_id);
                UnitTime utsre = expUpdBookService.getUnitTime(exp_id, step_id, step_pr, unit_id, unit_pr);

                List<OrderTime> ots = new ArrayList<>();
                for (ResMessage rm : rms) {
                    OrderTime ot = new OrderTime();
                    ot.unit_id_str = unit_id_str;
                    ot.resource_type = ResourceType.valueOf(rm.getRes_type());
                    ot.resource_id = rm.getRes_id();
                    ot.num = rm.getRes_num();
                    ot.startTime = unit_time;
                    ot.hour = utsre.getUnit_hour();

                    OrderTimeReply qtr = bookingTimeService.orderTime(ot);
                    ots.add(ot);
                    if (!qtr.isOK) {
                        expBookingService.changeExpStateById(uts.get(0).getExp_id(), "10050013");
                        for(OrderTime oti : ots) {
                            bookingTimeService.deleteTime(oti);
                        }
                        return false;
                    }
                }
            }
            for (SubmitUnitTime ut : uts) {
                int exp_id = ut.getExp_id();
                String exp_id_str = "EXP" + ut.getExp_id();
                int step_id = ut.getStep_id();
                int step_pr = ut.getStep_pr();
                int unit_id = ut.getUnit_id();
                String unit_id_str = ut.getUnit_id_str();
                int unit_pr = ut.getUnit_pr();
                String unit_time_str = ut.getUnit_time();
                Date unit_time = ut.getUnit_date();

                SimpleProInfo spi = new SimpleProInfo();

                spi.setExp_id(exp_id_str);
                spi.setPro_id(unit_id_str);
                spi.setPro_state("尚未开始");
                spi.setPro_start_time(unit_time);

                StringBuilder equ_info= new StringBuilder("[");
                StringBuilder animal_info= new StringBuilder("[");
                StringBuilder drug_info= new StringBuilder("[");
                StringBuilder sample_info= new StringBuilder("[");
                boolean a=true,b=true,c=true,d=true;

                List<ResMessage> expMessage = expBookingService.getUnitAllMaterialMessage(exp_id, step_id, step_pr, unit_id);

                for (ResMessage rm:expMessage) {
                    if (rm.getRes_type().equals(ResourceType.EQUIPMENT.toString())) {
                        String s = getResString(rm.getRes_id(), rm.getRes_name(), rm.getRes_num());
                        if (a) { s = s.substring(1); a=false;}
                        equ_info.append(s);
                    } else if (rm.getRes_type().equals(ResourceType.ANIMAL.toString())) {
                        String s = getResString(rm.getRes_id(), rm.getRes_name(), rm.getRes_num());
                        if (b) { s = s.substring(1); b=false;}
                        animal_info.append(s);
                    } else if (rm.getRes_type().equals(ResourceType.MATERIAL.toString())) {
                        String s = getResString(rm.getRes_id(), rm.getRes_name(), rm.getRes_num());
                        if (c) { s = s.substring(1); c=false;}
                        drug_info.append(s);
                    } else {
                        String s = getResString(rm.getRes_id(), rm.getRes_name(), rm.getRes_num());
                        if (d) { s = s.substring(1); d=false;}
                        sample_info.append(s);
                    }
                }

                spi.setEqu_info(equ_info+"]");
                spi.setAnimal_info(animal_info+"]");
                spi.setDrug_info(drug_info+"]");
                spi.setSample_info(sample_info+"]");

                ExpInfo expInfo = expInfoMapper.getExpInfo(exp_id, step_id, step_pr, unit_id, unit_pr);

                spi.setPi_id(expInfo.pi_id+"");
                spi.setAssist_id(expInfo.as_id+"");
                spi.setProject_id("PRO"+expInfo.pj_id);
                spi.setPro_name(expInfo.unit_name);
                spi.setExp_name(expInfo.exp_name);
                spi.setProject_name(expInfo.pj_name);

                simpleProInfoService.insertNewRecord(spi);
            }
            expBookingService.changeExpStateById(uts.get(0).getExp_id(), "19999990");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            expBookingService.changeExpStateById(uts.get(0).getExp_id(), "10050023");
            return false;
        }
    }

    public String getResString(int id, String name, int num) {
        return ",{\n" +
                "    \"id\": \""+id+"\",\n" +
                "    \"name\": \""+name+"\",\n" +
                "    \"need_num\": "+num+",\n" +
                "    \"retn_num\": 0,\n" +
                "    \"lost_num\": 0,\n" +
                "    \"app_status\": null,\n" +
                "    \"ret_status\": null,\n" +
                "    \"available_time1\": null,\n" +
                "    \"available_time2\": null,\n" +
                "    \"other_info\": {}\n" +
                "}\n";
    }
}
