package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.caulife.lab.entity.AddStep;
import top.caulife.lab.mapper.AddStepMapper;

@Service
public class AddStepService {
    @Autowired
    AddStepMapper addstepMapper;

    public void addstepjs(int stepno, String stepname,String stepmess){
        addstepMapper.addstepjs(stepno, stepname, stepmess);
    }
}
