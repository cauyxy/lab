package top.caulife.lab.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.bookingMessage.ExpMessage;
import top.caulife.lab.entity.bookingMessage.ResMessage;
import top.caulife.lab.entity.bookingMessage.UnitMessage;
import top.caulife.lab.entity.bookingTime.ResourceType;
import top.caulife.lab.entity.bookingTime.StepIden;
import top.caulife.lab.entity.bookingTime.UnitIden;
import top.caulife.lab.entity.bookingTime.UnitState;
import top.caulife.lab.mapper.ExpMesMapper;
import top.caulife.lab.mapper.ResourceMapper;
import top.caulife.lab.mapper.UnitIdenMapper;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExpBookingService {
    @Autowired
    ExpMesMapper expMesMapper;
    @Autowired
    UnitIdenMapper unitIdenMapper;
    @Autowired
    ResourceMapper resourceMapper;

    public List<ExpMessage> getExpMessage(int user_id, int user_type_id){
        if (user_type_id == 3 || user_type_id == 4)
            return expMesMapper.getExpMessageById(user_id);
        else if (user_type_id == 1 || user_type_id == 2)
            return expMesMapper.getAllExpMessage();
        else
            return new ArrayList<>();
    }

    public boolean delExpById(int experiment_id){
        try{
            expMesMapper.delExpById(experiment_id);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean changeExpStateById(int experiment_id, String progress_id){
        try{
            expMesMapper.changeExpStateById(experiment_id, progress_id);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean changeExpStateById(String exp_id, String progress_id){
        int experiment_id = Integer.parseInt(exp_id.substring(3));
        return changeExpStateById(experiment_id, progress_id);
    }

    public List<StepIden> getStepIdenById(int experiment_id){
        return unitIdenMapper.getStepIden(experiment_id);
    }

    public List<UnitMessage> getUnitIdenMessage(int experiment_id, int step_id, int step_priority){
        List<UnitIden> uis = unitIdenMapper.getUnitIden(experiment_id, step_id, step_priority);
        if (uis==null) return new ArrayList<>();
        List<UnitMessage> ums = new ArrayList<>();
        for (UnitIden ui : uis) {
            UnitMessage um = new UnitMessage();
            um.id = ui.getUnit_id();
            um.name = ui.getUnit_name();
            um.priority = ui.getUnit_priority();
            UnitState us = unitIdenMapper.getUnitState(UnitIden.getId(ui));
            if (us!=null) {
                um.progress_id = us.getProgress_id();
                um.progress_description = us.getProgress_description();
                um.progress_type = us.getProgress_type();
            }
            ums.add(um);
        }
        return ums;
    }

    public List<ResMessage> getUnitStaticMaterialMessage(int unit_id) {
        List<ResMessage> rms = new ArrayList<>();
        for (ResMessage rm : resourceMapper.getUnitStaticEquipmentMessage(unit_id)){
            rm.setMust(true);
            rm.setRes_type(ResourceType.EQUIPMENT.name());
            rms.add(rm);
        }
        for (ResMessage rm : resourceMapper.getUnitStaticMaterialMessage(unit_id)){
            rm.setMust(true);
            rm.setRes_type(ResourceType.MATERIAL.name());
            rms.add(rm);
        }
        return rms;
    }

    public List<ResMessage> getUnitDynamicMaterialMessage(int exp_id, int step_id, int step_priority, int unit_id) {
        List<ResMessage> rms = new ArrayList<>();
        for (String rm_type : new String[]{
                ResourceType.MATERIAL.name(),
                ResourceType.ANIMAL.name(),
                ResourceType.SAMPLE.name()
        }){
            for (ResMessage rm : resourceMapper.getUnitDynamicResMessage(exp_id, step_id, step_priority, unit_id, rm_type)){
                rm.setMust(false);
                rm.setRes_type(rm_type);
                rms.add(rm);
            }
        }
        return rms;
    }

    public List<ResMessage> getUnitAllMaterialMessage(int exp_id, int step_id, int step_priority, int unit_id) {
        List<ResMessage> rms = this.getUnitStaticMaterialMessage(unit_id);
        rms.addAll(this.getUnitDynamicMaterialMessage(exp_id, step_id, step_priority, unit_id));
        return rms;
    }
}
