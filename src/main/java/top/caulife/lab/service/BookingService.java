package top.caulife.lab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.entity.bookingMessage.ResMessage;
import top.caulife.lab.entity.bookingMessage.UnitTime;
import top.caulife.lab.entity.bookingTime.*;

import java.util.ArrayList;
import java.util.List;


@Service
public class BookingService {
    @Autowired
    ExpBookingService expBookingService;
    @Autowired
    BookingTimeService bookingTimeService;
    @Autowired
    ExpUpdBookService expUpdBookService;


    public TimeSlot queryTime(int exp_id, int step_id, int step_pr, int unit_id, int unit_pr) {
        List<ResMessage> rms = expBookingService.getUnitAllMaterialMessage(exp_id, step_id, step_pr, unit_id);
        UnitTime uts = expUpdBookService.getUnitTime(exp_id, step_id, step_pr, unit_id, unit_pr);
        TimeSlot tm = new TimeSlot();
        boolean first = true;
        for (ResMessage rm : rms) {
            QueryTime qt = new QueryTime();
            qt.setUnit_id_str(exp_id, step_id, step_pr, unit_id, unit_pr);
            qt.resource_type = ResourceType.valueOf(rm.getRes_type());
            qt.resource_id = rm.getRes_id();
            qt.num = rm.getRes_num();
            qt.hour = uts.getUnit_hour();

            QueryTimeReply qtr = bookingTimeService.queryTime(qt);
            if (first) {
                tm = tm.addUnion(qtr.timeSlot);
                first = false;
            } else {
                tm = tm.addInter(qtr.timeSlot);
            }
        }
        return tm;
    }

}
