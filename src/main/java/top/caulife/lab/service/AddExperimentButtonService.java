package top.caulife.lab.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.caulife.lab.mapper.AddExperimentButtonMapper;

@Service
public class AddExperimentButtonService {
    @Autowired
    AddExperimentButtonMapper addexperimentbuttonMapper;

    public void addexperimentbuttonjs(String projectno, String projectname, int experimentno, String experimentname, int PIno, int assistantno){
        addexperimentbuttonMapper.addexperimentbuttonjs(projectno, projectname, experimentno, experimentname, PIno, assistantno);
    }

    public void delexperimentbuttonjs(int experimentno){
        addexperimentbuttonMapper.delexperimentbuttonjs(experimentno);
    }
}
